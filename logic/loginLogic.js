import range from 'lodash/range';
import map from 'lodash/map';
import forEach from 'lodash/forEach';
import { createLogic  } from 'redux-logic';
import { REQUEST_LOGIN, LOADING_LOGIN, SUCCESS_LOGIN, ERROR_LOGIN, REQUEST_SIGNUP } from '../actions';
import { URL } from '../variables';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

const loginLogic = createLogic({
    type: REQUEST_LOGIN,
    latest: true,
    process({ getState, action }, dispatch, done){

        console.log(email);
        console.log(password);


        fetch(URL+'authenticate', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: action.email,
                password: action.password
            })
        }).then((response) => {

            response = JSON.parse(response._bodyText);

            if(response.success){
                dispatch({
                    type: SUCCESS_LOGIN,
                    token: response.token
                });
            } else {
                Alert.alert(response.message);
                dispatch({ type: ERROR_LOGIN });
            }
            done();
        }).catch((error)=>{
            console.log(error);
            Alert.alert('Problema na conexão!');
            done();
        });
    }
});

export default [loginLogic];


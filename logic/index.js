import { applyMiddleware  } from 'redux';
import { createLogic, createLogicMiddleware  } from 'redux-logic';
import concat from 'lodash/concat';
import loginLogic from './loginLogic';

const logicMiddleware = createLogicMiddleware(loginLogic, {});

export default applyMiddleware(logicMiddleware);



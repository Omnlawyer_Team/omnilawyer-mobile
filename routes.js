import Init from './pages/init';
import Login from './pages/login';
import Signup from './pages/signup';
import Home from './pages/home';

const routes = [
    {
        key: 'init',
        component: Init,
        hideNavBar: true,
        title: 'Página Inicial'
    },
    {
        key: 'login',
        component: Login,
        hideNavBar: true,
        title: 'Login'
    },
    {
        key: 'signup',
        component: Signup,
        hideNavBar: true,
        title: 'Cadastro'
    },
    {
        key: 'home',
        component: Home,
        title: 'Omnlawyer',
       // initial:true
    }
];


export default routes;

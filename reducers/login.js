import { Actions } from 'react-native-router-flux';
import { SUCCESS_LOGIN, SUCCESS_SIGNUP, REQUEST_LOGIN } from '../actions';

const defaultState = {
    token: null
};

export default function loginState(state = defaultState, action) {
    switch (action.type) {
        case REQUEST_LOGIN:
            return { ...state,
                loading: true
            };
        case SUCCESS_LOGIN:
            Actions.home({
            email:action.email
        });

            return { ...state,
                token: action.token,
                email: action.email
            };
        case SUCCESS_SIGNUP:
            Actions.login();
            return {...state,
                flag:1 };
    default:
        return state;
    }
}


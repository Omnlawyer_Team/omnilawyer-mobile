import { combineReducers, createStore   } from 'redux';
import logicMiddleware from '../logic';
import login from './login';

const reducer = combineReducers({ login });
//const store = createStore(reducer, logicMiddleware);
const store = createStore(reducer);
export default store;

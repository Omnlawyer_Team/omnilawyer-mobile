import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { Router, Scene } from 'react-native-router-flux';
import store from './reducers';
import routes from './routes';
import { Font  } from 'exponent';
import fontRoboto from './public/fonts/Roboto-Medium.ttf';

export default class App extends React.Component {

    componentDidMount() {
    }
    render() {

        return (
            <Provider store={store}>
                <Router>
                    <Scene key="root">
                        { routes.map((route)=><Scene {...route}/>)  }
                    </Scene>
                </Router>
            </Provider>
        );
    }
}


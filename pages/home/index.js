import { connect  } from 'react-redux';
import Home from './Home';

const mapStateToProps = (state) => {
    console.log(state);
    return { login: state.login };
}

const mapDispatchToProps = (dispatch) => {
    return {};
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);


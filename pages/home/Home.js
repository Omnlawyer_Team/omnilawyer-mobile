import React from 'react';
import { TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Container, Content, Body, Button, Text, Picker, Item, Card, CardItem } from 'native-base';
import { Col, Row, Grid  } from 'react-native-easy-grid';
//import { DatePicker } from 'react-native-datepicker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { URL } from '../../variables';
import map from 'lodash/map';
import Datacard from './Datacard';


const style = {
};


export default class Home extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: [],
            isVoid: false,
            email: props.login.email
        };

    }

    componentDidMount(){

        const email = this.props.email || this.state.email;

        fetch(URL + 'citacoes', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email
            })
        }).then((response) => {
            console.log("#1", response);
            response = JSON.parse(response._bodyText);
            console.log("#2", response);
            if(response.success){
                this.setData({ data: response.data })
            } else {
                Alert.alert(response.message);
            }
        }).catch((error)=>{
            console.log(error);
            Alert.alert('Problema na conexão!');
        });

    }

    getData(){
        return map(this.state.data, (item)=>{
            return <Datacard {...item}/>
        });
    }


    render(){
        const showData = this.getData();
        return (
            <Container>
                <Content>
                    <ScrollView>
                        {showData || 'Não foram encontrados dados'}
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}

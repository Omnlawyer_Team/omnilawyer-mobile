import React from 'react';
import { TouchableOpacity, ScrollView, Alert } from 'react-native';
import { Container, Content, Body, Button, Text, Picker, Item, Card, CardItem } from 'native-base';


export default class Datacard extends React.Component {

    render(){
        const titulo = this.props.titulo;
        const minititle = title.substr(20) + '...';
        const diario = this.props.diario;
        const conteudo = this.props.conteudo;

        return (
            <Card>
                <CardItem header>
                    <Text>{minititle}</Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Text>{titulo}</Text>
                        <Text>{conteudo}</Text>
                    </Body>
                </CardItem>
                <CardItem footer>
                    <Text>CJPI - {diario}</Text>
                </CardItem>
            </Card>
        );
    }
}


import React from 'react';
import { Image } from 'react-native';

import { Actions  } from 'react-native-router-flux';
import { Container, Content, Button, Text  } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';

import logoImage from "../../public/images/logo-with-name-320.png";

const style = {
    image: {
        marginTop: 120,
        width: 170,
        height: 170,
        marginBottom: 60,
    },
    container: {
        backgroundColor: '#FFFBF7'
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        backgroundColor: '#522A24',
        width: '80%',
        justifyContent: 'center'
    },
    buttonText: {
        textAlign: 'center'
    },
    subButton: {
        marginTop: 10,
        color: '#2B1717',
        textAlign: 'center'
    }
};


export default class Init extends React.Component {

    goLogin(){ Actions.login(); }
    goSignup(){ Actions.signup(); }

    render(){
        return (
            <Container>
                <Content>
                    <Grid style={style.content}>
                            <Row size={1}>
                                <Image
                                    style={style.image}
                                    source={logoImage}/>
                            </Row>
                            <Row size={4}>
                                <Button rounded style={style.button} onPress={this.goLogin}>
                                    <Text style={style.buttonText}>Entrar</Text>
                                </Button>
                            </Row>
                            <Row size={1}>
                                <Button transparent onPress={this.goSignup}>
                                    <Text style={style.subButton}>Não é cadastrado? Clique aqui</Text>
                                </Button>
                            </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}



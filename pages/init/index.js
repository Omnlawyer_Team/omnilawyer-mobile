import { connect  } from 'react-redux';
import Init from './Init';

const mapStateToProps = (state) => {
     return { };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toNextWeek: () => {
            dispatch({
                                type: 'SCHEDULE_NEXT_WEEK'
                                            
            });
                    
        },
        toLastWeek: () => {
            dispatch({
                                type: 'SCHEDULE_LAST_WEEK'
                                            
            });
                    
        }
           
    }

}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Init);



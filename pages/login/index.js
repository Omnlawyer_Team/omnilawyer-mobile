import { connect  } from 'react-redux';
import Login from './Login';
import { REQUEST_LOGIN, SUCCESS_LOGIN, ERROR_LOGIN } from '../../actions';
import { URL } from '../../variables';
import { Alert } from 'react-native';

const mapStateToProps = (state) => {
    return { };
}

const mapDispatchToProps = (dispatch) => {
    return {
        requestLogin:(email, password) => {

            fetch(URL+'authenticate', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            }).then((response) => {

                response = JSON.parse(response._bodyText);

                if(response.success){
                    dispatch({
                        type: SUCCESS_LOGIN,
                        token: response.token,
                        email: email
                    });
                } else {
                    Alert.alert(response.message);
                }
            }).catch((error)=>{
                console.log(error);
                Alert.alert('Problema na conexão!');
            });

            dispatch({
                type: REQUEST_LOGIN,
                email: email,
                password: password,
            });

        }
    }

}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);


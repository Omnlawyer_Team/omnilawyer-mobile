import React from 'react';
import { Alert } from 'react-native';
import { Container, Content, H2, Form, Item, Input, Button, Text, Icon } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { Actions } from 'react-native-router-flux';

const style = {
    image: {
        width: 50,
        height: 50
    },
    backButton: {
        marginTop: 20
    },
    backButtonColor: {
        color: '#522A24'
    },
    form: {
        alignItems: 'center'
    },
    header: {
        textAlign: 'center',
        color: '#2B1717',
        margin: 20
    },
    loginButton: {
        backgroundColor:'#522A24',
        marginTop: 20
    }
};


export default class Login extends React.Component {

    goInit(){
        Actions.init();
    }

    handleEmail(email){
        this.setState({
            email: email
        })
    }

    handlePassword(password){

        this.setState({
            password: password
        });
    }

    goLogin(){
        const email = this.state.email;
        const password = this.state.password;

        if( email === "" || password === "")
            return Alert.alert('Campos em branco','Preencha todos os campos');
        this.props.requestLogin(email, password);
    }

    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: ""
        };
    }

    render(){
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <Col>
                                <Button iconLeft transparent onPress={this.goInit} style={style.backButton}>
                                    <Icon name='arrow-back' style={style.backButtonColor}/>
                                    <Text style={style.backButtonColor}>Voltar</Text>
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <H2 style={style.header}>Login</H2>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form>
                                    <Item>
                                        <Input
                                            value={this.state.email}
                                            onChangeText={(email) => this.setState({email})} placeholder="Email"/>
                                    </Item>
                                    <Item>
                                        <Input value={this.state.password}
                                        onChangeText={(password)=>this.setState({password})} placeholder="Senha" secureTextEntry={true}/>
                                    </Item>
                                    <Button rounded style={style.loginButton} onPress={this.goLogin.bind(this)}>
                                        <Text>Entrar</Text>
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}




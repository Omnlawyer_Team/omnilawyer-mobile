import React from 'react';
import { Container, Icon, Content, Button, Form, Item, H2, Input, Picker, Text  } from 'native-base';
import { Image, TextInput } from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import map from 'lodash/map';
import { Actions } from 'react-native-router-flux';

const style = {
    image: {
        width: 50,
        height: 50
    },
    backButton: {
        marginTop: 20
    },
    backButtonColor: {
        color: '#522A24'
    },
    form: {
        alignItems: 'center'
    },
    header: {
        textAlign: 'center',
        color: '#2B1717',
        margin: 20
    },
    loginButton: {
        backgroundColor:'#522A24',
        marginTop: 20
    }
};


export default class Signup extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            name: '',
            estado: '',
            oab_num: '',
            email: '',
            password: ''
        };
    }

    onSelectUf(){
        console.log(this, arguments);
    }

    goSignup(){
        const name = this.state.name;
        const estado = this.state.estado;
        const oab_num = this.state.oab_num;
        const email = this.state.email;
        const password = this.state.password;

        this.props.requestSignup(name, 'pi', oab_num, email, password);
    }
    goInit(){
        Actions.init();
    }


    render(){
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <Col>
                                <Button iconLeft transparent onPress={this.goInit} style={style.backButton}>
                                    <Icon name='arrow-back' style={style.backButtonColor}/>
                                    <Text style={style.backButtonColor}>Voltar</Text>
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <H2 style={style.header}>Cadastro</H2>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form>
                                    <Item>
                                        <Input value={this.state.name} onChangeText={(name)=>this.setState({name})} placeholder="Nome"/>
                                    </Item>
                                    <Item>
                                        <Input onChangeText={(oab_num)=>this.setState({oab_num})} value={this.state.oab_num} placeholder="Número da OAB" keyboardType="numeric"/>
                                    </Item>
                                    <Item>
                                        <Input value={this.state.email} onChangeText={(email)=>{this.setState({email})}} placeholder="Email"/>
                                    </Item>
                                    <Item>
                                        <Input onChangeText={(password)=>this.setState({password})} value={this.state.password} placeholder="Senha" secureTextEntry={true}/>
                                    </Item>
                                    <Button rounded style={style.loginButton} onPress={this.goSignup.bind(this)}>
                                        <Text>Cadastrar</Text>
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}




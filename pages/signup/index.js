import { connect  } from 'react-redux';
import Signup from './Signup';
import { URL } from '../../variables';
import { SUCCESS_SIGNUP } from '../../actions';

const mapStateToProps = (state) => {
     return { };
}

const mapDispatchToProps = (dispatch) => {
    return {
        requestSignup: (name, estado, oab_num, email, password)=>{

            fetch(URL+'signUp', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ name, estado, oab_num, email, password })
            }).then((response) => {

                response = JSON.parse(response._bodyText);
                console.log(response);

                if(response.success){
                    dispatch({
                        type: SUCCESS_SIGNUP,
                    });
                    fetch(URL+'notification', {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            flag:'1', email,name,oab_num
                        })
                    });
                } else {
                    Alert.alert(response.message);
                }
            }).catch((error)=>{
                console.log(error);
                Alert.alert('Problema na conexão!');
            });


        }
    }

}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Signup);



